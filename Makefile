# include makefile targets from the submodule
include .make/oci.mk
# include core makefile targets for release management
-include .make/base.mk
# include your own private variables for custom deployment configuration
-include PrivateRules.mak
