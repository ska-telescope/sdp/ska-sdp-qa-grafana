# ska-sdp-qa-grafana
## Description
This Grafana instance is used as an alternative QA Metrics display for the purpose of testing and exploring data from QA data API's. 

# Table of contents

- [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)
- [Project status](#project-status)

# Installation
[(Back to top)](#table-of-contents)

>Note: The Grafana instance is currently tested to work with the qa-data-api commit fe6e862e, and the qa-metric-generator commit faadade5. 

Installing in a new/clean environment:
1. Clone this repository into your environment:
```sh
git clone git@gitlab.com:ska-telescope/sdp/ska-sdp-qa-grafana.git
```
2. Copy the initial Grafana ini file into your project from the resources folder in the repository:
```sh
scp ./resources/grafana.ini ./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/configuration/
```
3. Run the Grafana container:
```sh
cd ska-sdp-qa-grafana/
docker-compose up -d
```
4. Install the required WebSocket plugin:
```sh
docker exec -it ska-sdp-qa-grafana bash
grafana-cli plugins install golioth-websocket-datasource
grafana-cli plugins install natel-plotly-panel
docker-compose down
docker-compose up -d

```
5. Log into Grafana with user: admin, password: admin, you can then change it is required.

6. Navigate to data sources, open the ska-qa-data-api data source and add the host address: "ws://qa-data-api:8002/ws/consumer/", click Save and Test. (if it fails, Save and Test again.) NOTE: This is an unfortunate work around and not persistent. After the container has been restarted, this step needs to be repeated. 

7. Navigate to the Spectrum Frequencies dashboard, this should now be connected to the DATA API if it is running. Depending on your time zone, you might need to update your time range to overlap with UTC to view generated data.



# Usage
[(Back to top)](#table-of-contents)

## Starting the Grafana instance
1. Run the Grafana container:
```sh
cd ska-sdp-qa-grafana/
docker-compose up -d
```
2. Log into Grafana with your user (or use the default user: admin, password: admin).

3. Navigate to data sources, open the ska-qa-data-api data source and add the host address: "ws://qa-data-api:8002/ws/consumer/", click Save and Test. (if it fails, Save and Test again.) NOTE: This is an unfortunate work around and not persistent. After the container has been restarted, this step needs to be repeated. 

4. Navigate to the Spectrum Frequencies dashboard, this should now be connected to the DATA API if it is running. Depending on your time zone, you might need to update your time range to overlap with UTC to view generated data.

## Provisioning
https://grafana.com/docs/grafana/latest/administration/provisioning/

Included data sources and dashboards are defined via files that can be version controlled by using Grafana provisioning. The procedure for making changes to these files are as follows:

## Updating an existing dashboard

Predefined dashboards json files live in the ./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/dashboards/ folder and is provisioned in the ./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/configuration/provisioning/dashboards/. To make persistent changes to the dashboards:
1. Navigate to the dashboard in the browser, make the required changes and save the dashboard. 
2. Open the dashboard settings and copy the json structure of the update dashboard. 
3. Past the json structure over the original file (/ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/dashboards/)
4. Commit you changes to the repo.

## Adding a new Dashboard
Predefined dashboards json files live in the ./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/dashboards/ folder and is provisioned in the ./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/configuration/provisioning/dashboards/. To make persistent changes to the dashboards:
1. Create a new dashboard in the browser, make the required changes and save the dashboard. 
2. Open the dashboard settings and copy the json structure of the update dashboard. 
3. Past the json structure in a new file with a file named after the dashboard name (/ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/dashboards/)
4. Add a new yaml config file to provision the dashboard (./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/configuration/provisioning/dashboards/)
5. Commit you changes to the repo.

## Adding a new data source
1. Add a new yaml config file to provision the new data source (./ska-sdp-qa-grafana/src/ska-sdp-qa-grafana/configuration/provisioning/datasources/ 
2. Commit you changes to the repo.


# Support
[(Back to top)](#table-of-contents)

If you get stuck, the SKA Slack channel #team-naledi is good places to go for help or reporting any issues.

# Roadmap
[(Back to top)](#table-of-contents)

We aim to add new metric displays and update the existing dashboards as API's, information and requirements become available.

# Contributing
[(Back to top)](#table-of-contents)

We are open to contributions.

# License
[(Back to top)](#table-of-contents)

Please see LICENSE file.

# Project status
[(Back to top)](#table-of-contents)

* The Grafana instance is currently tested to work with the qa-data-api commit fe6e862e, and the qa-metric-generator commit faadade5. 
* The following metrics have been tested up to a draft dashboard:
    * Synthetic / Randomly Generate Data (Spectrum & Phase plot)
    * Measurement set Data (ms_to_qa)
* The following metrics have not yet been implemented:
    * RFI debugging
    * ska-sdp-cbf-emulator Integration
